extends MeshInstance

class_name Case

enum TUILES {
	plaine, graines, bois, pierres, ville, 
	champ, scierie, carriere, 
	entrepot0, entrepot1, entrepot2
}

export(TUILES) var typeId:int
var type:String 
var batiment

# RESSOURCES {population, nourriture, bois, pierre}
var ressources:Array
# warning-ignore:unused_class_variable
export var ressource1:Array = [0,-1]
# warning-ignore:unused_class_variable
export var ressource2:Array = [0,-1]
# warning-ignore:unused_class_variable
export var ressource3:Array = [0,-1]

func _ready():
	if(typeId == 4):
		self.owner.villes.append(self)
	ressources = [ressource1.duplicate(), ressource2.duplicate(), ressource3.duplicate()]
	type = TUILES.keys()[typeId]
	actuSprite()

func actuSprite():
	if(batiment):
		$BatimentsPossibles.get_node(self.batiment.sprite).visible = true

func triggerTour():
	if(batiment):
		batiment.effetTour(self)


func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed == true:
			if($"../..".selectedCase != self):
				$Surbrillance.visible = true
				$"../..".changeSelection(self)
			else :
				$"../..".annulerSelection()

func desactiver_surbrillance():
	$Surbrillance.visible = false

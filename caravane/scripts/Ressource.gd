extends Area2D

func _ready():
	$Sprite.region_rect = Rect2(0, 64 * (self.get_position_in_parent()-1), 64, 64)
	yInitPos = $RevenuEffet.rect_position.y

func actuRessource(qqt:int):
	$Label.text = String(qqt)
	$Label.set("custom_colors/font_color", Color(1,0,0) if qqt<=0 else Color(0,0,0))

func actuRevenu(qqt:int):
	$Tooltip/Revenu.text = ("+" if qqt>0 else "") + String(qqt) + "/tour"
	$Tooltip/Revenu.set("custom_colors/font_color", Color(0,1,0) if qqt>0 else Color(1,0,0) if qqt<0 else Color(0,0,0))

func actuStockage(qqt:int):
	$Tooltip/Stockage.text = "max : " + String(qqt)



func _on_Ressource_mouse_entered():
	$Tooltip.visible = true

func _on_Ressource_mouse_exited():
	$Tooltip.visible = false



var revenuEffetTimer:float = -1
var yInitPos:int

func afficherRevenu(qqt:int):
	if(qqt!=0):
		$RevenuEffet.visible = true
		revenuEffetTimer = .5
		$RevenuEffet.rect_position.y = yInitPos
		$RevenuEffet.text = String(qqt)
		$RevenuEffet.set("custom_colors/font_color", Color(0,1,0) if qqt>0 else Color(1,0,0))
	
func _physics_process(delta):
	if(revenuEffetTimer != -1):
		revenuEffetTimer-=delta
		$RevenuEffet.rect_position.y += 1
		if(revenuEffetTimer < 0):
			revenuEffetTimer = -1
			$RevenuEffet.visible = false

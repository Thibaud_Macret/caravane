extends Spatial

var tour:int = 1
var selectedCase:Object = null
var ressources:Array = Globals.ressources.duplicate() #[2, 10, 0, 0]
var revenu:Array = Globals.ressources.duplicate() #[1, -1, 0, 0]
var stockage:Array = Globals.ressources.duplicate() #[10, 10, 10, 10]
# warning-ignore:unused_class_variable
var villes:Array

func _ready():
	$Hud/InfoTerrain.visible = false
	
	selectedCase = $Cases/VilleInit
	construire(3)
	
	init_ressources()
	actuHUD()

func init_ressources():
	ressources[0] = 2
	ressources[1] = 10
	#ressources = [100,100,100,100] #God Mod
	revenu[0] = 1
	revenu[1] = -1
	for index in range(stockage.size()):
		stockage[index] = 10



#Gestion du tour
func _physics_process(delta):
	if(Input.is_action_just_pressed("passe_tour")):
		passe_tour()

func passe_tour():
	tour += 1
	$Hud/Tour/Label.text = "Tour " + String(tour)
	annulerSelection()
	for tuile in ($Cases.get_children()):
		tuile.triggerTour()
	revenus()
	actuRessourcesActions()

func revenus():
	for indexRessource in ressources.size():
		var income = revenu[indexRessource]
		if(ressources[indexRessource] + income > stockage[indexRessource]):
			income = stockage[indexRessource] - ressources[indexRessource]
		ressources[indexRessource] += income
		$Hud/Ressources.get_child(indexRessource+1).afficherRevenu(income)
	



#Gestion des ressources
func actuHUD() -> void:
	actuRessourcesActions()
	actuRevenus()
	actuStockage()

func actuRessourcesActions() -> void:
	for index in range(ressources.size()):
		$Hud/Ressources.get_child(index+1).actuRessource(ressources[index])
	actuActions()
	
func actuActions() -> void:
	$Hud/InfoTerrain/InfoConstruction.visible = false
	if(selectedCase):
		for index in range($Hud/InfoTerrain/Constructions.get_child_count()):
			$Hud/InfoTerrain/Constructions.get_child(index).disabled = !(Globals.batiments[index].conditions(selectedCase) &&
				check_ressources(index) && selectedCase.type in Globals.batiments[index].terrain)
	else:
		for index in range($Hud/InfoTerrain/Constructions.get_child_count()):
			$Hud/InfoTerrain/Constructions.get_child(index).disabled = true

func actuRevenus():
	for index in range(revenu.size()):
		$Hud/Ressources.get_child(index+1).actuRevenu(revenu[index])

func actuStockage():
	for index in range(stockage.size()):
		$Hud/Ressources.get_child(index+1).actuStockage(stockage[index])



#Selction d'une tuile
func changeSelection(case:Object) -> void :
	if(selectedCase):
		selectedCase.desactiver_surbrillance()
	selectedCase = case
	actuActions()
	$Hud/InfoTerrain.visible = true
	for index in case.ressources.size():
		if(case.ressources[index][1] > -1):
			$Hud/InfoTerrain/Ressources.get_child(index).get_child(0).text = String(case.ressources[index][1])
			$Hud/InfoTerrain/Ressources.get_child(index).get_child(1).region_rect = Rect2(0, 64 * case.ressources[index][0], 64, 64)
		$Hud/InfoTerrain/Ressources.get_child(index).visible = !(case.ressources[index][1]==-1)

func annulerSelection():
	$Hud/InfoTerrain.visible = false
	if(selectedCase):
		selectedCase.desactiver_surbrillance()
	selectedCase = null
	actuActions()

func _on_FondArea_input_event(camera, event, click_position, click_normal, shape_idx):	
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed :
			annulerSelection()



#Actions joueur
func construire(batimentId:int):
	var batiment:Batiment = Globals.batimentsInstances[batimentId].new()
	for indexRessource in ressources.size():
		ressources[indexRessource] -= batiment.cout[indexRessource]
	selectedCase.batiment = batiment
	selectedCase.actuSprite()
	batiment.effetArrivee(selectedCase)
	annulerSelection()
	actuRessourcesActions()

func check_ressources(batimentId:int) -> bool:
	for indexRessource in ressources.size():
		if(Globals.batiments[batimentId].cout[indexRessource] > ressources[indexRessource]):
			return false
	return true


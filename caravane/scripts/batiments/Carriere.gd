extends "res://scripts/batiments/BatimentProduction.gd"

func _init():
	nom = "Carrière"
	description = ("Augmente la production de pierre de 1\n"+
	"Doit être placé dans un montagne\n"+
	"Consomme 1 pierre par tour")
	#{population, nourriture, bois, pierre}
	cout[0] = 1
	cout[1] = 1
	cout[2] = 2
	#{plaine, graines, bois, pierres}
	terrain = ["pierres"]
	sprite = "Carriere"

func conditions(case:Case) ->bool :
	if(distanceVille(case) > 3):
		return false
	return true

func effetArrivee(case:Case):
	effetReprod(case)

func effetReprod(case:Case):
	case.owner.revenu[3] += 1
	case.owner.actuRevenus()

func effetTour(case:Case):
	produire(3, case)

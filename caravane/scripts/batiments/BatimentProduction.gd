extends "res://scripts/batiments/Batiment.gd"

var epuise:bool = false

func produire(ressource:int, case:Case) -> void :
	var indexRessourceCase:int = fetchRessourceIndex(case.ressources,ressource)
	if(indexRessourceCase == -1): #la ressource n'existe pas sur la case
		epuiser(ressource, case)
	elif(case.ressources[indexRessourceCase][1] > 0) : #la ressource est en qqt suffisante
		if(epuise):
			effetReprod(case)
			self.epuise = false
		case.ressources[indexRessourceCase][1] -= 1
	elif(!self.epuise): #la ressource est épuisée
		epuiser(ressource, case)

func epuiser(ressource:int, case:Case) -> void :
	self.epuise = true
	case.owner.revenu[ressource] -= 1
	case.owner.actuRevenus()

func effetReprod(case:Case):
	pass #Pour réactiver la production d'un bâtiment

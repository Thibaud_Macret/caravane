extends "res://scripts/batiments/Batiment.gd"

func _init():
	nom = "Entrepôt à bois"
	description = ("Augmente les stocks de bois de 10\n"+
	"Doit être placé sur une case libre\n")
	#{population, nourriture, bois, pierre}
	cout[0] = 1
	cout[2] = 5
	cout[3] = 5
	#{plaine, graines, bois, pierres}
	terrain = ["plaine"]
	sprite = "Entrepot0"

func conditions(case:Case) ->bool :
	if(distanceVille(case) > 3):
		return false
	return true

func effetArrivee(case:Case):
	case.owner.stockage[2] += 10
	case.owner.actuStockage()

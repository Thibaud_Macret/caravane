extends "res://scripts/batiments/BatimentProduction.gd"

func _init():
	nom = "Scierie"
	description = ("Augmente la production de bois de 1\n"+
	"Doit être placé sur une forêt\n"+
	"Consomme 1 bois par tour")
	#{population, nourriture, bois, pierre}
	cout[0] = 1
	cout[1] = 3
	#{plaine, graines, bois, pierres}
	terrain = ["bois"]
	sprite = "Scierie"

func conditions(case:Case) ->bool :
	if(distanceVille(case) > 3):
		return false
	return true

func effetArrivee(case:Case):
	effetReprod(case)

func effetReprod(case:Case):
	case.owner.revenu[2] += 1
	case.owner.actuRevenus()

func effetTour(case:Case):
	produire(2, case)

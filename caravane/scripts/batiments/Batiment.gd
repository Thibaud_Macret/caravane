extends Node

class_name Batiment

# warning-ignore:unused_class_variable
var nom:String
# warning-ignore:unused_class_variable
var description:String
#{population, nourriture, bois, pierre}
# warning-ignore:unused_class_variable
var cout:Array = Globals.ressources.duplicate()
#{plaine, graines, foret, montagne, ville, champ, scierie, carriere}
# warning-ignore:unused_class_variable
var terrain:Array
# warning-ignore:unused_class_variable
var sprite:String

func effetArrivee(case:Case):pass
func effetTour(case:Case):pass
func conditions(case:Case)->bool:return true

func distanceVille(case:Case)->int:
	var distance:int = 100
	for ville in case.owner.villes :
# warning-ignore:narrowing_conversion
		distance = min(distance, int( 
			(abs(ville.translation.x - case.translation.x)/6.4) + 
			(abs(ville.translation.z - case.translation.z)/6.4) ) )
	return distance

func fetchRessourceIndex(tableau:Array, ressource:int) ->int :
	for index in tableau.size():
		if(tableau[index][0] == ressource):
			return index
	return -1

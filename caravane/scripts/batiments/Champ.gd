extends "res://scripts/batiments/BatimentProduction.gd"

func _init():
	nom = "Champ"
	description = ("Augmente la production de blé de 1\n"+
	"Doit être placé sur une case fertile\n"+
	"Consomme 1 blé par tour")
	#{population, nourriture, bois, pierre}
	cout[0] = 1
	cout[1] = 1
	#{plaine, graines, bois, pierres}
	terrain = ["graines"]
	sprite = "Champ"

func conditions(case:Case) ->bool :
	if(distanceVille(case) > 3):
		return false
	return true

func effetArrivee(case:Case):
	case.get_node("Sprite").visible = false
	effetReprod(case)

func effetReprod(case:Case):
	case.owner.revenu[1] += 1
	case.owner.actuRevenus()

func effetTour(case:Case):
	produire(1, case)

extends "res://scripts/batiments/Batiment.gd"

func _init():
	nom = "Ville"
	description = ("Produit un travailleur par tour et etend votre influence\n"+
	"Doit être placé sur une case libre, à plus de trois cases d'une ville\n"
	+"Consomme une nourriture par tour")
	#{population, nourriture, bois, pierre}
	cout[0] = 3
	cout[1] = 5
	cout[2] = 5
	cout[3] = 5
	#{plaine, graines, bois, pierres}
	terrain = ["plaine"]
	sprite = "Ville"

func conditions(case:Case) ->bool :
	if(distanceVille(case) < 3 || distanceVille(case) > 5):
		return false
	return true

func effetArrivee(case:Case):
	case.owner.villes.append(case)
	print(case.owner.villes)
	case.owner.revenu[0] += 1
	case.owner.revenu[1] -= 2
	case.owner.actuRevenus()

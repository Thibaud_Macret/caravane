extends TextureButton

export var construId:int
var hover:bool
var hoverTimer:float

func _ready():
# warning-ignore:return_value_discarded
	self.connect("pressed", self.owner, "construire", [construId])
	$Sprite.set_texture(load("res://imgs/bouttonsConstru/constru" + String(construId) + ".png"))


func _on_BtnConstruction_mouse_entered():
	hover = true

func _on_BtnConstruction_mouse_exited():
	$"../../InfoConstruction".visible = false
	hover = false
	hoverTimer = 0

func _physics_process(delta):
	if(hover):
		hoverTimer += delta
		if(hoverTimer > .5):
			$"../../InfoConstruction".visible = true
			change_tooltip()
			hover = false

func change_tooltip():
	$"../../InfoConstruction/Nom".text = Globals.batiments[construId].nom
	$"../../InfoConstruction/Description".text = Globals.batiments[construId].description
	$"../../InfoConstruction/Cout/Res0/Label".text = String(Globals.batiments[construId].cout[0])
	$"../../InfoConstruction/Cout/Res1/Label".text = String(Globals.batiments[construId].cout[1])
	$"../../InfoConstruction/Cout/Res2/Label".text = String(Globals.batiments[construId].cout[2])
	$"../../InfoConstruction/Cout/Res3/Label".text = String(Globals.batiments[construId].cout[3])

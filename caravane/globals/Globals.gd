extends Node

#{population, nourriture, bois, pierre}
const ressources = [0,0,0,0]

# warning-ignore:unused_class_variable
onready var batiments = [
	load("res://scripts/batiments/Champ.gd").new(),
	load("res://scripts/batiments/Scierie.gd").new(),
	load("res://scripts/batiments/Carriere.gd").new(),
	load("res://scripts/batiments/Ville.gd").new(),
	load("res://scripts/batiments/Entrepot0.gd").new(),
	load("res://scripts/batiments/Entrepot1.gd").new(),
	load("res://scripts/batiments/Entrepot2.gd").new()
]

# warning-ignore:unused_class_variable
onready var batimentsInstances = [
	preload("res://scripts/batiments/Champ.gd"),
	preload("res://scripts/batiments/Scierie.gd"),
	preload("res://scripts/batiments/Carriere.gd"),
	preload("res://scripts/batiments/Ville.gd"),
	preload("res://scripts/batiments/Entrepot0.gd"),
	preload("res://scripts/batiments/Entrepot1.gd"),
	preload("res://scripts/batiments/Entrepot2.gd")
]
